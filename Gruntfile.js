/*global module:false*/
module.exports = function(grunt) {

  path = require('path');

  require('time-grunt')(grunt);

  // load all grunt tasks
  require('load-grunt-tasks')(grunt);
  

  // Project configuration.
  grunt.initConfig({

    config: {
      tdinventory:  '../todoist-inventory',
      lessFiles:  'todoist/Todoist/todoist/apps/app_platform/**/*.less',
      coffeeFiles:  'todoist/Todoist/todoist/apps/app_platform/**/*.coffee',
    },
    
    less: {
      development: {
          files: [
            {
              expand: true,
              src: ['<%= config.lessFiles %>'],
              ext: '.css',
              rename: function(dest, src) {
                return path.dirname(src) + "/" + ".less." + path.basename(src)
              }
            },
          ],
      }
    },

    coffee: {
      compile: {
        options: {
          bare: true
        },
        files: [
          {
            expand: true,
            src: ['<%= config.coffeeFiles %>'], 
            ext: '.js',
            rename: function(dest, src) {
              return path.dirname(src) + "/" + ".coffee." + path.basename(src)
            }
          },
        ]
      }
    },

    shell: {
        reloadtodoist: {
            command: 'supervisorctl -c <%= config.tdinventory %>/supervisor.conf restart todoist worker'
        }
    },

    watch: {
      todoist: {
        files: ['**/*.py', '**/*.mako'],
        tasks: ['shell:reloadtodoist']
      },

      less: {
        files: ['<%= config.lessFiles%>'],
        tasks: ['less']
      },

      coffee: {
        files: ['<%= config.coffeeFiles %>'],
        tasks: ['coffee']
      },
    },

  });


  // Default task.
  grunt.registerTask('default', ['compile', 'watch']);
  grunt.registerTask('compile', ['coffee', 'less']);

};
