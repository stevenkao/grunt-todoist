
# Environment Setup

- [Node.js](http://nodejs.org/)
- [Grunt](http://gruntjs.com/)



# Installation

```
npm install
```


# Get Started

```
grunt
```
